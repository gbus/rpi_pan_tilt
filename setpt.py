from pantilt import PanTilt
import yaml
import sys
from pathlib import Path

config_path = "{}/.setpt".format(str(Path.home()))
pantilt_conf = '{}/pantilt.yaml'.format(config_path)


def main():
    try:
        f = open(pantilt_conf, 'r')
        pt_c = yaml.safe_load(f)
    except IOError:
        print("Error: Configuration file can't be opened.")
        sys.exit(-1)

    pt_arg = sys.argv[1]

    try:
        pan_val = pt_c['pantilt']['views'][pt_arg][0]
        tilt_val = pt_c['pantilt']['views'][pt_arg][1]
    except KeyError as err:
        print("Invalid view or view configuration. Error: '{}'".format(err))
        sys.exit(-2)

    PCA9685_conf = pt_c['pantilt']['config_file'].format(config_path)
    pt = PanTilt(
        pt_c['pantilt']['pan_pwm_ch'],
        pt_c['pantilt']['tilt_pwm_ch'],
        PCA9685_conf
    )

    pt.point(pan_val, tilt_val)


if __name__ == "__main__":
    main()